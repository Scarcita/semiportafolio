import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {

  forma! : FormGroup;

  constructor(private fb: FormBuilder) {
    this.crearFormulario();
  
    
    this.forma = this.fb.group({
      nombre: ['',[ Validators.required, Validators.minLength(4), Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      apellido: ['', [Validators.required, Validators.minLength(4),  Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      email: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      celular: ['', [Validators.required, Validators.pattern(/^[0-9]+$/)]],
      mensaje: ['', [Validators.required, Validators.maxLength(250), Validators.pattern(/^[a-zA-zñÑ\s]+$/) ]]
      
    });
   }

  ngOnInit(): void {

  }
   
  crearFormulario(): void {
  }

  guardar(): void{
    if(!this.forma.valid){
      return
    }
    this.limpiarFormulario();
  }


  get nombreNoValido() {
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched;
  }

  get apellidoNoValido(){
    return this.forma.get('apellido')?.invalid && this.forma.get('apellido')?.touched;
  }

  get correoNoValido(){
    return this.forma.get('email')?.invalid && this.forma.get('email')?.touched;
  }

  get celularoNoValido(){
    return this.forma.get('celular')?.invalid && this.forma.get('celular')?.touched;
  }

  get mensajeoNoValido(){
    return this.forma.get('mensaje')?.invalid && this.forma.get('mensaje')?.touched;
  }


    limpiarFormulario(): void{
  //this.forma.reset();
  this.forma.reset({
    //nombre: 'Pedro'
  });
}



}
